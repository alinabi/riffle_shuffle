#!/usr/bin/env python3

import sys
import csv
import math
import numpy as np
import numpy.random as rnd
import scipy.stats as stats

#
# the following two functions implement
# the deck cutting and card interleaving
# strategies for the Gilbert-Shannon-Reeds
# riffle shuffle
#

def gsr_cut(deck_size):
  return rnd.binomial(deck_size, 0.5)


def gsr_drop(left_size, right_size):
  if left_size == 0:
    return True
  else:
    theta = float(right_size) / float(left_size + right_size)
    return rnd.choice([False, True], p=[1 - theta, theta], replace=True)



# brand new deck, please!
def new_deck(size):
  return np.arange(0, size, dtype=np.dtype('i8'))  
  


# this simulates shuffling the deck once with the specified strategies
def shuffle(deck, get_random_number_for_right_deck=gsr_cut, should_drop_from_right_deck=gsr_drop):
  n = len(deck)
  k = get_random_number_for_right_deck(n)
  left_size, right_size = n - k, k
  new_deck = np.empty((n,), dtype=np.dtype('i8'))
  index = n - 1
  while index >= 0:
    if should_drop_from_right_deck(left_size, right_size):
      new_deck[index] = deck[n - k + right_size - 1]
      right_size = right_size - 1
    else:
      new_deck[index] = deck[left_size - 1]
      left_size = left_size - 1
    index = index - 1
  return new_deck


#
# shuffles a deck a specified number of times,
# i.e. runs one Markov chain on permutations,
# keeping all intermediate results
#
def run_chain(deck_size, iter=20, **kwargs):
  deck = new_deck(deck_size)
  chain = []
  for i in range(iter): 
    deck = shuffle(deck, **kwargs)
    chain.append(deck)
  return chain


#
# run multiple Markov chains, keeping all the resuts
#
def run_sim(deck_size, chains=10000, iter=20):
  res = []
  checkpoints = [float(i + 1)/10.0 for i in range(10)]
  for n in range(chains):
    chain = run_chain(deck_size, iter=iter)
    res.append(chain)
    if float(n + 1)/float(chains)  >= checkpoints[0]:
      print('{} chains done ({}%)'.format(n + 1, checkpoints[0] * 100), file=sys.stderr)
      checkpoints = checkpoints[1:]
  return np.array(res, dtype=np.dtype('i8'))



#
# applies a binomial test to the empirical distribution
# of the number of fixed points obtained from a simulation run
#
def test_fixed_point_freqs(sim_res):
  n_chains, n_shuffles, deck_size = sim_res.shape
  obs = np.zeros((n_shuffles, deck_size + 1), dtype=np.dtype('i8'))
  for i in range(n_chains):
    for j in range(n_shuffles):
        obs[j, count_fixed_points(sim_res[i, j, :])] += 1
  probs = compute_fixed_point_ref_freqs(deck_size)
  result = [multinomial_test(obs[k, :], simplex=probs, draws=n_chains) for k in range(n_shuffles)]
  return result


#
# applies a binomial test to the empirical distribution
# of the top card obtained from a simulation run
#
def test_top_card_freqs(sim_res):  
  n_chains, n_shuffles, deck_size = sim_res.shape
  obs = np.zeros((n_shuffles, deck_size), dtype=np.dtype('i8'))
  for i in range(n_chains):
    for j in range(n_shuffles):
      obs[j, sim_res[i, j, 1]] += 1
  probs = compute_top_card_ref_freqs(deck_size)
  result = [multinomial_test(obs[k, :], simplex=probs, draws=n_chains) for k in range(n_shuffles)]
  return result


#
# applies a binomial test to the empirical distribution
# of the number of rising sequences obtained from a simulation run
#
def test_rising_seq_freqs(sim_res):
  n_chains, n_shuffles, deck_size = sim_res.shape
  obs = np.zeros((n_shuffles, deck_size), dtype=np.dtype('i8'))
  for i in range(n_chains):
    for j in range(n_shuffles):
      obs[j, count_rising_sequences(sim_res[i, j, :]) - 1] += 1
  probs = compute_rising_seq_ref_freqs(deck_size)
  result = [multinomial_test(obs[k, :], simplex=probs, draws=n_chains) for k in range(n_shuffles)]
  return result


def count_fixed_points(perm):
  return (np.arange(len(perm), dtype=np.dtype('i8')) == perm).sum()


def invert_permutation(perm):
  return np.argsort(perm)


def count_descents(perm):
  return np.sum(perm[:-1] > perm[1:])


def count_rising_sequences(perm):  
  return 1 + count_descents(invert_permutation(perm))



#
# the following two functions are computed recursively,
# using infinite precision arithmetic, so they are expensive.
# we mitigate this by using memoization to trade memory for speed
#
def eulerian_number(n, m):
  if n <= 0 or m < 0 or m >= n:
    return 0
  if n < len(eulerian_number.memo):
    return eulerian_number.memo[n][m]
  new_row = [(n - k) * eulerian_number(n - 1, k - 1) + (k + 1) * eulerian_number(n - 1, k) for k in range(n)]
  eulerian_number.memo.append(new_row)
  return new_row[m]

eulerian_number.memo = [[0], [1]]      


def factorial(n):
  if n < 0:
    return 0
  if n < len(factorial.memo):
    return factorial.memo[n]
  result = n * factorial(n - 1)
  factorial.memo.append(result)
  return result

factorial.memo = [1]





#
# the next three functions compute the 
# theoretical probability distributions
# for the three statistics under consideration
#

def compute_rising_seq_ref_freqs(deck_size):
  freqs = [eulerian_number(deck_size, m) / factorial(deck_size) for m in range(deck_size)]
  return np.array(freqs, dtype=np.dtype('f8'))


def compute_fixed_point_ref_freqs(deck_size):
  res = []
  for m in range(deck_size + 1):
    terms = ((1 - ((k & 1) << 1)) / factorial(k) for k in range(deck_size - m + 1))
    res.append(math.fsum(terms) / factorial(m))   
  return np.array(res, dtype=np.dtype('f8'))


def compute_top_card_ref_freqs(deck_size):
  return np.repeat(1.0/deck_size, deck_size)




#
# just what it says on the label
# see for example https://en.wikipedia.org/wiki/Multinomial_test
#
def multinomial_test(data, simplex=None, draws=10000):
  if simplex is None:
    simplex = np.repeat(1.0 / len(data), len(data))
  if len(data) != len(simplex):
    raise ValueError('argument lengths do not match')
  dist = stats.multinomial(n=sum(data), p=simplex)
  sample = dist.logpmf(dist.rvs(size=draws))
  cutoff = dist.logpmf(data)
  pval = float((sample <= cutoff).sum()) / draws
  return pval





def main(alpha=0.01):
  statistical_tests = [
    {'name': 'Top Card', 'func': test_top_card_freqs},
    {'name': 'Fixed Points', 'func': test_fixed_point_freqs},
    {'name': 'Rising Sequences', 'func': test_rising_seq_freqs}
  ]
  deck_sizes = [26, 52, 104]
  for size in deck_sizes:
    print('Performing shuffle simulation for a {} card deck'.format(size), file=sys.stderr)
    data = run_sim(size)
    print('Performing statistical tests', file=sys.stderr)
    pvals = []
    for test in statistical_tests:
      print('Current test: {}'.format(test['name']), file=sys.stderr)
      pvals.append(test['func'](data))
    result = [(i,) + row for (i, row) in enumerate(zip(*pvals), 1)]
    headers = ['Num Shuffles'] + [test['name'] for test in statistical_tests]
    output_file ='p_values_{}_card_deck.csv'.format(size) 
    with open(output_file, 'w') as f:
      wrt = csv.writer(f)
      wrt.writerow(headers)
      wrt.writerows(result)
    print('Results written to {}'.format(output_file), file=sys.stderr)


if __name__ == '__main__':
  main()
