% Riffle Shuffles Randomness Testing
% Alex Constandache
% October 22, 2018

\newcommand\eulerian[2]{\genfrac{\langle}{\rangle}{0pt}{}{#1}{#2}}

# The riffle shuffle

A riffle shuffle is performed by cutting the deck into two packs and then 
interleaving the cards from the two packs. Different choices of cutting
and interleaving strategies, which can be either deterministic or stochastic,
result in different shuffles. From a mathematical point of view, a riffle 
shuffle is a Markov chain on the space $\mathcal{S}_n$ of permutations of 
$n$ elements. We are trying to find out how many steps the Markov chain needs 
to take before the distribution over permutations is sufficiently close to
uniform.

Since even for a half deck the number of permutations is enormous ($26! \approx 4 \times
10^{26}$), we cannot verify the uniformity of the distribution directly. Even after 
millions of runs, we would still probably not see the same permutation twice, unless the 
deviation from the uniform distribution was really severe. Instead, we need to construct 
a test statistic, a function $t$ on the space of permutations which has the following
properties:

* it takes values in a discrete set of cardinality $m \ll n!$
* if $\Sigma$ is a uniformly distributed, permutation valued random variable, then
  the distribution of $t(\Sigma)$ can be computed analytically.


In this work we propose three such test statistics and investigate their effectiveness
by numerical simulation.


# Top card

The simplest test statistic we could try is the value of the top card _after_ the shuffle, 
assuming we start with an ordered deck. For a permutation $\sigma$, this statistics is defined
to be $t(\sigma) = \sigma(1)$. The statistic can take $n$ possible value, and if $\sigma$ is 
drawn from a uniform distribution then all $n$ values are equally likely, because exactly
$(n - 1)!$ out of $n!$ permutations have a prescribed card $k$ on top.


# Fixed points

A _fixed point_ for a permutation $\sigma$ is a position $i$ such that $\sigma(i) = i$. A 
permutation can have between $0$ and $n$ fixed points. The number of permutations of $n$ 
elements with exactly $k$ fixed points is called as the _rencontres number_ and is denoted
by $D_{n, k}$. These numbers can be computed using the following formula:
\begin{equation}
D_{n, k} = \frac{n!}{k!}\sum_{j = 0}^{n - k} \frac{(-1)^k}{k!}
\end{equation}
Therefore, if we draw a permutation uniformly at random, the probability that it would have 
exactly $k$ fixed points is $\frac{D_{n, k}}{n!}$.


# Rising sequences

A permutation $\sigma$ ha a _descent_ at position $i$ is a if $\sigma(i) > \sigma(i + 1)$. 
The number of permutation of $n$ elements with exactly $k$ descents is given by the Eulerian 
number $\eulerian{n}{k}$, which can be computed recursively using the following recursion 
relations:
\begin{equation}
    \eulerian{n}{0} = 1, \quad 
    \eulerian{n}{n} = 0, \quad 
    \eulerian{n}{k} = (n - k) \eulerian{n - 1}{k - 1} + (k + 1) \eulerian{n - 1}{k}.
\end{equation}   
Thus the probability that a perturbation drawn uniformly at random has exactly $k$ descents
is $\frac{1}{n!}\eulerian{n}{k}$.

A sequence of positions $i_1 < i_2 < \dots < \i_m$ is a _rising sequence_ of $\sigma$ if
$\sigma(i_1) < \sigma(i_2) < \dots < \sigma(i_m)$, but $\sigma(i) < sigma(i_m)$ for all
$i > i_m$. A permutation can have between 1 and $n$ rising sequences. The number of rising
sequences in a permutation is one greater than the number of descents in its inverse.

We define this statistic in terms of rising sequences rather than descents, because riffle
shuffles have a very simple effect on the number of rising sequences. If one starts with an 
ordered deck and performs one riffle shuffle, each of the two packs will become a rising 
sequences in the resulting permutation, unless the packs are merely concatenated, in which 
case we get only one rising sequence. So, after one shuffle the number of rising sequences
can be at most 2. Similarly, after 2 shuffles we can have at most 4 rising sequences, after
3 shuffles we can have at most 8, and so on, until this upper bound reaches the maximum number 
of rising sequences, which is $n$.

Because of its relationship with the riffle shuffle, this statistic should be very effective
at detecting deviations from the uniform distribution over permutations after a small
number of shuffles. Also, since small perturbations in the shuffle, such as dropping more than
one card at once from the same pack, do not affect the number of rising sequences, the conclusions
drawn using it should be pretty robust. 

# Multinomial test

Once we are equipped with a test statistic $t$, we perform a large number of shuffle simulations
and count how many times we observe each outcome of the $t$. Let $\mathbf{x}$ be this vector of 
counts and let $P(\mathbf{x})$ be the probability this observation according to a multinomial
distribution parameterized by the theoretical probability distribution over the outcomes 
of $t$. The $p$-value of our observation is the probability of observing a vector of counts that
is at most as likely as $x$:

\begin{equation}
p = \sum_{\{\mathbf{y} | P(\mathbf{y}) < P(\mathbf{x})\}} P(\mathbf{y})
\end{equation}

We compute this value by Monte Carlo sampling. We reject the null hypothesis when this $p$-value
is less than 0.01. The minimum number of shuffles required to randomize the deck is the lowest
number of shuffles after which we can no longer reject the null hypothesis that all permutations
are equally likely.


# Results

Shuffles    Top Card    Fixed Points    Rising Sequences
---------   ---------   -------------   -----------------
1           **0.0**     **0.0**         **0.0**
2           **0.0**     **0.0**         **0.0**
3           **0.0**     **0.0**         **0.0**
4           **0.0**     **0.0**         **0.0**
5           **0.0**     **0.0003**      **0.0**
6           **0.0**     0.0014          **0.0**
7           0.0181      0.0367          **0.0**
8           0.474       0.6965          **0.0**
9           0.7816      0.3459          **0.0**
10          0.1641      0.0818          **0.0018**
11          0.252       0.1585          0.4485
12          0.0093      0.3243          0.1646
13          0.4984      0.6462          0.0962
14          0.822       0.7949          0.896
15          0.7292      0.846           0.3084
16          0.5421      0.3769          0.1286
17          0.4956      0.1512          0.7688
18          0.5726      0.1567          0.4895
19          0.7927      0.2335          0.5082
20          0.1423      0.8697          0.6883
-----------------------------------------------------------

Table: p-values under the null hypothesis that all permutations are
       equally likely, for a 26 card deck. The values in bold are 
       the cases where we can reject the null with 99% confidence


Shuffles    Top Card    Fixed Points    Rising Sequences
---------   ---------   -------------   -----------------
1           **0.0**     **0.0**         **0.0**
2           **0.0**     **0.0**         **0.0**
3           **0.0**     **0.0**         **0.0**
4           **0.0**     **0.0**         **0.0**
5           **0.0**     **0.0**         **0.0**
6           **0.0**     0.0257          **0.0**
7           0.0708      0.6725          **0.0**
8           0.3022      0.3244          **0.0**
9           0.941       0.9779          **0.0**
10          0.6003      0.8279          **0.0**
11          0.4227      0.6464          **0.0**
12          0.7101      0.5571          0.037
13          0.7216      0.0829          0.4742
14          0.893       0.3688          0.2067
15          0.8848      0.9599          0.7592
16          0.9916      0.3792          0.1757
17          0.0205      0.1953          0.7115
18          0.6594      0.2556          0.4236
19          0.8549      0.6986          0.8119
20          0.3773      0.8809          0.862
-----------------------------------------------------------

Table: p-values for 52 card deck


Shuffles    Top Card    Fixed Points    Rising Sequences
---------   ---------   -------------   -----------------
1           **0.0**     **0.0**         **0.0**
2           **0.0**     **0.0**         **0.0**
3           **0.0**     **0.0**         **0.0**
4           **0.0**     **0.0**         **0.0**
5           **0.0**     **0.0**         **0.0**
6           **0.0**     0.4037          **0.0**
7           0.0578      0.4695          **0.0**
8           0.6713      0.4027          **0.0**
9           0.9234      0.3811          **0.0**
10          0.0511      0.9868          **0.0**
11          0.5983      0.66            **0.0**
12          0.8608      0.9691          **0.0**
13          0.0309      0.9283          0.0853
14          0.3067,     0.4648          0.5237
15          0.3316      0.2293          0.6199
16          0.994       0.6303          0.1747
17          0.1419      0.5556          0.4468
18          0.2399      0.4693          0.3968
19          0.5969      0.5096          0.4777
20          0.9708      0.0547          0.3916
-----------------------------------------------------------

Table: p-values for 104 card deck
